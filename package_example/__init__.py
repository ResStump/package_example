# This init file is the python script that is include when one imports the package

# To import functions from python scripts that are located in the same directory
from .functions import * # to call function as <package>.<function> (other is also possible)
from . import functions2 # to call function as <package>.functions2.<function>

# To import code from an other folder
from . import other_folder