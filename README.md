# Example package
An example package, which can be used as template to create a package.

## Installation

To install the package run
```bash
pip install git+https://gitlab.com/ResStump/package_example.git
```

Or if you want to install it from the local git repository run
```bash
pip install .
```
in the folder of the git repositry


## Example

```python
import package_example as pe

# functions from package_example directory
pe.add(1, 2)
# or
pe.functions.add(1, 2)
pe.functions2.mul(1, 2) # pe.mul(1, 2) is not possible


pe.other_folder.message("test")
pe.other_folder.some_func2.message2("test2")
```