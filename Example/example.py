# %%
import package_example as pe

# functions from package_example directory
pe.add(1, 2)
# or
pe.functions.add(1, 2)
pe.functions2.mul(1, 2) # pe.mul(1, 2) is not possible


pe.other_folder.message("test")
pe.other_folder.some_func2.message2("test2")

# %%